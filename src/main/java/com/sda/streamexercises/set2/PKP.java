package com.sda.streamexercises.set2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class PKP {
    private List<Pociag> pociags = new ArrayList<>();

    public PKP(List<Pociag> pociags) {
        this.pociags = pociags;
    }

    public void wypiszWszystkiePociagi() {
//        pociags.stream().forEach(pociag -> System.out.println(pociag));
//        pociags.stream().forEach(System.out::println);
        pociags.forEach(System.out::println);

        List<Pociag> innaLista = new ArrayList<>();
        pociags.forEach(PKP::doSomethingWithPociag);
    }

    private static void doSomethingWithPociag(Pociag p) {
        System.out.println("!!!!$!#$@#%");
        System.out.println(p);
        System.out.println("!!!!$!#$@#%");
        System.out.println("!!!!$!#$@#%");
    }

    public void wypiszWszystkieEkonomicznePociągi() {
//        pociags.stream()
//                .filter((pociag -> {
//                    return pociag.getKlasa() == KlasaPociagu.EKONOMIA;
//                }))
//                .forEach(System.out::println);
        pociags.stream()
                .filter((pociag -> pociag.getKlasa() == KlasaPociagu.EKONOMIA))
                .forEach(System.out::println);
    }

    public List<Pociag> zwróćPociągiKlasy(KlasaPociagu klasaPociagus) {
        return pociags.stream()
                .filter((pociag -> pociag.getKlasa() == klasaPociagus))
                .collect(Collectors.toList());
    }

    public Optional<Pociag> zwróćNajbardziejOpóźniony() {
        return pociags.stream()
                .sorted(((o1, o2) -> Integer.compare(o1.getMaxOpóźnienie(), o2.getMaxOpóźnienie())))
                .findFirst();
    }

    public List<Pociag> zwróć3NajbardziejOpóźnione(){
        return pociags.stream()
                .sorted(((o1, o2) -> Integer.compare(o1.getMaxOpóźnienie(), o2.getMaxOpóźnienie())))
                .limit(3)
                .collect(Collectors.toList());
    }

    public Optional<Pociag> zwróćNajbardziejOpłacalny(){
        return pociags.stream()
                .sorted((o1, o2) -> Double.compare((o1.getCenaPodróży()/o1.getDługośćPodróży()), o2.getCenaPodróży()/o2.getDługośćPodróży()))
                .findFirst();
    }

    public List<Pociag> zwróć3NajbardziejOpłacalne(){
        return pociags.stream()
                .sorted((o1, o2) -> Double.compare((o1.getCenaPodróży()/o1.getDługośćPodróży()), o2.getCenaPodróży()/o2.getDługośćPodróży()))
                .limit(3)
                .collect(Collectors.toList());
    }

    public void wypiszPociągiPosortowanymiNumeramiPociągu(){
        pociags.stream()
                .sorted((o1, o2) -> (o1.getIdentyfikator().compareTo(o2.getIdentyfikator())))
                .forEach(pociag -> System.out.println(pociag));
    }

//    public Set<KlasaPociagu> zwróćWszystkieDostępneKlasyPociągów(){
//        return pociags.stream()
//                .map(pociag -> pociag.getKlasa())
//                .collect(Collectors.toSet());
//    }

    public List<KlasaPociagu> zwróćWszystkieDostępneKlasyPociągów(){
        return pociags.stream()
                .map(pociag -> pociag.getKlasa())
                .distinct()
                .collect(Collectors.toList());
    }

    public List<Pociag> zwróćKlasyBiznesZSypialnianymi(){
        return pociags.stream()
                .filter(pociag -> pociag.getKlasa() == KlasaPociagu.BIZNES)
                .filter(pociag -> pociag.isCzyPosiadaWagonSypialny())
                .collect(Collectors.toList());
    }
}
